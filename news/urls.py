from rest_framework import routers
from .views import NewsViewSet


router = routers.DefaultRouter()
router.register('api/news', NewsViewSet, 'news')

urlpatterns = router.urls
