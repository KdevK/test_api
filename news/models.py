from django.db import models

# Create your models here.

class News(models.Model):
    id = models.BigAutoField(primary_key=True)
    title = models.CharField(verbose_name='Заголовок', max_length=150)
    description = models.CharField(verbose_name='Тело', max_length=10000)
    image = models.ImageField(verbose_name='Картинка', blank=True)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Дата обновления')

    class Meta:
        verbose_name = 'Новость'
        verbose_name_plural = 'Новости'